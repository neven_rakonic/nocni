from django.contrib import admin
from nocnired.models import TimeTable


class TimeTableAdmin(admin.ModelAdmin):
    list_display = ('tram_num', 'direction', 'day', 'time_insert')
    list_filter = ['tram_num', 'direction', 'day']

admin.site.register(TimeTable, TimeTableAdmin)
