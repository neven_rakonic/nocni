from django.conf.urls import patterns, url
from nocnired import views

urlpatterns = patterns('',
    url(r'^$', views.home, name='home'),
    url(r'^(\d{2})/$', views.tram, name='tram'),
    url(r'^(\d{2})/(\w+)/$', views.display_times, name='direction'),    
    url(r'^(\d{2})/(\w+)/(\w+)/$', views.display_times, name='day'),
    url(r'^(\d{2})/(\w+)/(\w+)/(\d{1,2}_\d{2})/$', views.display_times, name='time'), 
       
)