from django.db import models

class TimeTable(models.Model):
    tram_num = models.CharField(max_length=3)   
    day = models.CharField(max_length=15)
    direction = models.CharField(max_length=20) 
    time_insert = models.TimeField(auto_now=False, auto_now_add=False)

    def __unicode__(self):
        return "{0}   {1}   {2}".format(
            str(self.tram_num), str(self.time_insert),
            str(self.day))
