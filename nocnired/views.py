# -*- coding: UTF-8 -*-
import re
from django.shortcuts import render, redirect
from django.utils.http import urlquote
from nocnired.models import TimeTable



#HELPER FUNCTIONS

def parse_query(query):    
    query_dict = {}

    #cleans time from the query, because it's trickier to handle
    time_pattern = "\d{1,2}:\d{2}"                      #still needs more precise input
    time = re.findall(time_pattern, query)    
    if time:
        query_dict["time"] = time[0]

    clean_query = re.split(time_pattern, query)[0]  #re.split returns a list with 1 member  
    query = clean_query.lower().split()      

    direct_found = False

    num_list = ["31", "32", "33", "34"]
    direct_list = ["dubec", "savski most", "borongaj", u"črnomerec", "ljubljanica", u"žitnjak"]
    day_list = ["tjedan", "subota", "nedjelja"]

    for term in query:
        if term in num_list:
            query_dict["num"] = term
            continue
        if term in day_list:
            query_dict["day"] = term
            continue
        if not direct_found:
            direction = part_in_list(term, direct_list)
            if direction:
                query_dict["direction"] = direction[0]   #returned direction is a list with one element
                direct_found = True
    
    return query_dict

def part_in_list(term, term_list): 
    """
    matches partial strings in list
    """   
    match = [t for t in term_list if term in t]
    return match or False

def remove_whitespace(string):
    return string.replace(' ', '_')

def add_whitespace(string):
    return string.replace('_', ' ')

def change_to_colon(string):
    return string.replace('_',':')

def change_from_colon(string):
    return string.replace(':','_')

#VIEWS

def home(request):
    context_dict = {"error": "search has gone wrong!" }
    
    if request.method == "POST":          
        query = parse_query(request.POST["query"])

        if len(query) == 0:
            return render(request, 'nocnired/home.html', context_dict) 

        if query.get("num"):            
            end_url = "{0}/".format(query["num"])                           
        else:                                                               
            return render(request, 'nocnired/home.html', context_dict)

        if query.get("direction"):
            direction = remove_whitespace(query["direction"]).encode('utf-8')   #needed to avoid unicode problems        
            end_url += "{0}/".format(direction)
        else:
            return redirect(end_url)

        if query.get("day"):                      
            end_url += "{0}/".format(query["day"])
        else:
            return redirect(end_url)

        if query.get("time"):
            time = change_from_colon(query['time'])            
            end_url += "{0}/".format(time)
        
        end_url = urlquote(end_url)    #solves problems when turning unicode string to url        

        return redirect(end_url)

    return render(request, 'nocnired/home.html')            


def tram(request, tram_num):
    #maybe refactor this out, also check to see if passing only one list is enough
    context_dict = {}
    
    query = TimeTable.objects.filter(tram_num=tram_num).order_by('time_insert')
    
    #pulls out possible directions and days out of the queryset
    #the order_by at the end is used because otherwise distinct wouldn't work
    directions = query.values_list('direction', flat=True).distinct().order_by()
    days = query.values_list('day', flat=True).distinct().order_by()   
    
    first_direction = query.filter(direction=directions[0])
    second_direction = query.filter(direction=directions[1])

    results = []
    for day in days:
        results.append(first_direction.filter(day=day))

    results_t = [] 
    for day in days:
        results_t.append(second_direction.filter(day=day))   
   
    #print results_t
    context_dict = {"results": results, "results_t": results_t}

    return render(request, 'nocnired/tram.html', context_dict)

def display_times(request, *args):
    context_dict = {}
    
    tram_num = args[0]
    direction = args[1]
    direction = add_whitespace(direction)

    results = TimeTable.objects.filter(tram_num=tram_num).order_by('time_insert')
    results = results.filter(direction=direction.lower())

    if len(args) == 2:
        days = results.values_list('day', flat=True).distinct().order_by()
        end_result = []
        for day in days:
            end_result.append(results.filter(day=day))
        context_dict = {"results": end_result}   
        return render(request, 'nocnired/tram.html', context_dict)
    
    #day must be included if there are more than 2 args
    day = args[2]
    results = results.filter(day=day)

    if len(args) == 3:

        context_dict ={"results": results}
        return render(request, 'nocnired/home.html', context_dict)

    if len(args) == 4:
        time = args[3]
        time = change_to_colon(time)
        earlier_t = results.filter(time_insert__lte=time).order_by('-time_insert')[:2]
        later_t = results.filter(time_insert__gte=time)[:2]

        end_result = [inst for inst in earlier_t]
        end_result.reverse()
        end_result += [inst for inst in later_t]

        context_dict = {"results": end_result}
        return render(request, 'nocnired/home.html', context_dict)




  
    
