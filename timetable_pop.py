# -*- coding: UTF-8 -*-

import os
import random

def populate(direction_list):
    for num in range(3):
        for count in range(100):
            rhour = str(random.randint(0,8))
            rmin_first = str(random.randint(0,5))
            rmin_second = str(random.randint(0,9))
            time = "0{0}:{1}{2}".format(rhour, rmin_first, rmin_second)
            TimeTable(tram_num=35+num, 
                      time_insert = time,
                      day=random.choice(["tjedan", "subota","nedjelja"]), 
                      direction = direction_list[num][random.randint(0,1)]).save()


if __name__ == '__main__':    
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nocni.settings')
    from nocnired.models import TimeTable
    direction_list = [["dubec", "savski most"],["borongaj", "črnomerec"], ["ljubljanica", "žitnjak"]]
    populate(direction_list)




